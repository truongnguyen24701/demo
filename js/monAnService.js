const BASE_URL = "https://62bd9567bac21839b6069b0e.mockapi.io/mon-an";
export let monAnService = {
    layDanhSachMonAn: () => {
        return axios({
            url: BASE_URL,
            method: "GET",
        });
    },
    xoaMonAn: (id) => {
        return axios({
            url: `${BASE_URL}/${id}`,
            method: "DELETE",

        });
    },
    themMonAnMoi: (monAn) => {
        return axios({
            url: BASE_URL,
            method: "POST",
            // thêm món ăn phải có data của 
            data: monAn,
        });
    },
    layThongTinMonAn: (idMonAn) => {
        return axios({
            url: `${BASE_URL}/${idMonAn}`,
            method: "GET",
        })
    },
    capNhapMonAn: (monAn) => {
        return axios({
            url: `${BASE_URL}/${monAn.id}`,
            method: "PUT",
            data: monAn,
        })
    },
};
